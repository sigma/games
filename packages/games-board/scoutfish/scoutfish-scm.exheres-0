# Copyright 2021 Ali Polatel <alip@exherbo.org>
# Distributed under the terms of the GNU General Public License v2

# SCM_REPOSITORY="https://github.com/mcostalba/${PN}.git"
# pychess repository includes fix for ^^ issue 42 and more.
SCM_REPOSITORY="https://github.com/pychess/${PN}.git"
require scm-git

SUMMARY="Chess Query Engine"
HOMEPAGE="https://github.com/pychess/${PN}"
DOWNLOADS=""

LICENCES="GPL-3"
SLOT="0"
PLATFORMS="~amd64"
MYOPTIONS="
    debug
    optimize [[ description = [ Use CXXFLAGS as recommended by upstream ] ]]

    platform: amd64
    amd64_cpu_features: avx2 popcnt
"

# TODO(alip):
# Add python option to install scoutfish.py
# src_test requires python to execute src/test.py
#DEPENDENCIES="
#    build+run:
#        python? ( dev-python/pexpect:=[python_abis:*(-)?] )
#"

WORK=${WORK}/src

# TODO(alip):
# s{cout,tock}fish pretty much use identical Makefiles.
# Deduplicate common functions into an exlib.
scoutfish_arch() {
    local myarch=

    if option platform:amd64 ; then
        myarch=x86-64
        if option amd64_cpu_features:avx2; then
            myarch="${myarch}-bmi2"
        elif option amd64_cpu_features:popcnt; then
            myarch="${myarch}-modern"
        fi
    else
        die "${PN}: unsupported platform"
    fi

    echo "$myarch"
}

src_prepare() {
    default

    # prevent pre-stripping
    sed -e 's:-strip $(BINDIR)/$(EXE)::' -i Makefile \
        || die 'failed to disable stripping in the Makefile'
}

src_compile() {
    emake all \
        ARCH=$(scoutfish_arch) COMP=${CXX} COMPILER=${CXX} \
        debug=$(option debug yes no) \
        optimize=$(option optimize yes no)
}

src_install() {
    dobin ${PN}

    edo cd ..
    emagicdocs
}

