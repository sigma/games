# Copyright 2018 Heiko Becker <heirecka@exherbo.org>
# Distributed under the terms of the GNU General Public License v2

require cmake python [ blacklist=2 multibuild=false ]

export_exlib_phases src_prepare

SUMMARY="A program that automatically solves layouts of Freecell and similar variants"
DESCRIPTION="
It ca solve Freecell and similar variants of Card Solitaire such as Eight Off,
Forecell, and Seahaven Towers, as well as Simple Simon boards. Included with
the archive are programs that can automatically provide the starting layouts
of several popular Solitaire Implementations.
Freecell Solver can also be built as a library for use within your own
Solitaire implementations."

HOMEPAGE="http://fc-solve.shlomifish.org/"
DOWNLOADS="${HOMEPAGE}downloads/fc-solve/${PNV}.tar.xz"

LICENCES="
    MIT
    BSD-3 [[ note = [ kaz_tree.{c,h} ] ]]
"
SLOT="0"
MYOPTIONS=""

DEPENDENCIES="
    build:
        app-text/asciidoctor
        dev-libs/rinutils
        dev-util/gperf
        virtual/pkg-config
    build+run:
        dev-perl/Games-Solitaire-Verify
        dev-perl/Moo
        dev-perl/Path-Tiny
        dev-perl/Template-Toolkit
    build+test:
        dev-python/pysol-cards[python_abis:*(-)?]
        dev-python/random2[python_abis:*(-)?]
        dev-python/six[python_abis:*(-)?]
    test:
        dev-libs/gmp:*
        dev-perl/CHI
        dev-perl/Inline
        dev-perl/Perl-Tidy
        dev-perl/Task-FreecellSolver-Testing
        dev-perl/Test-Data-Split
        dev-perl/Test-Differences
        dev-perl/Test-RunValgrind
        dev-perl/Test-TrailingSpace
        dev-perl/Test-Trap
        dev-python/cffi[python_abis:*(-)?]
        dev-python/freecell-solver[python_abis:*(-)?]
        dev-python/pycotap[python_abis:*(-)?]
        dev-util/cmocka
"

UPSTREAM_DOCUMENTATION="${HOMEPAGE}docs/"

CMAKE_SRC_CONFIGURE_PARAMS+=(
    -DCMAKE_INSTALL_DOCDIR:PATH=/usr/share/doc/${PNVR}
    -DBUILD_STATIC_LIBRARY:BOOL=FALSE
    # Man pages are already included in the tarball, would need asciidoctor
    -DFCS_BUILD_DOCS:BOOL=FALSE
)
CMAKE_SRC_CONFIGURE_TESTS+=( '-DFCS_WITH_TEST_SUITE:BOOL=TRUE -DFCS_WITH_TEST_SUITE:BOOL=FALSE' )

freecell-solver_src_prepare() {
    cmake_src_prepare

    # Unbundle rinutils
    edo rm -rf rinutils/*

    edo sed -e "s|\(share/man/man\)|/usr/\1|" \
        -e "/SET (DATADIR/ s|\${CMAKE_INSTALL_PREFIX}|/usr|" \
        -i cmake/Shlomif_Common.cmake

    # Should probably use find_package(Python 3 COMPONENTS Interpreter)
    edo sed -e "s|\"python3\"|\"python$(python_get_abi)\"|" -i CMakeLists.txt

    edo sed -e "s|'python3'|'python$(python_get_abi)'|" -i t/t//transpose-board.py

    # Wants to use ag (dev-util/the_silver_search)
    edo rm t/t/spelling.t

    # A bit like -Werror, breaks all the time, more interesting to the author
    edo rm t/t/tidyall.t
}

