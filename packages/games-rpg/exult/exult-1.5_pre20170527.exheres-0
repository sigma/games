# Copyright 2010-2017 Wulf C. Krueger <philantrop@exherbo.org>
# Distributed under the terms of the GNU General Public License v2

require autotools [ supported_autoconf=[ 2.5 ] supported_automake=[ 1.15 1.14 1.13 ] ]

SUMMARY="A project to recreate Ultima 7 for modern operating systems using the original game data"
HOMEPAGE="http://${PN}.sourceforge.net"
DOWNLOADS="http://dev.exherbo.org/~philantrop/distfiles/${PNV}.tar.xz"

REMOTE_IDS="freecode:${PN} sourceforge:${PN}"

UPSTREAM_CHANGELOG="${HOMEPAGE}/snapshots/ChangeLog [[ lang = en ]]"
UPSTREAM_DOCUMENTATION="${HOMEPAGE}/docs.php [[ lang = en ]]"

LICENCES="GPL-2"
SLOT="0"
PLATFORMS="~amd64"
MYOPTIONS="
        alsa         [[ description = [ Enable ALSA midi support ] ]]
        debug
        exult-studio [[ description = [ Enable the Exult Studio editor, allowing you to customize the game ] ]]
        fluidsynth   [[ description = [ Enable fluidsynth midi support ] ]]
        gimp-plugin  [[ description = [ Build the GIMP plugin ] ]]
        mt32emu      [[ description = [ Enable built-in mt32emu support ] ]]
        opengl       [[ description = [ Enable EXPERIMENTAL OpenGL rendering support ] ]]
        timidity     [[ description = [ Enable built-in timidity midi support (*not* TiMidity++!) ] ]]
"
# timidity-0.2 from 1995 and mt32emu (last release in 2005) are bundled. Since
# there are no newer releases of either project and they don't install anything
# colliding with TiMidity++ or anything else, we keep both as an option.

DEPENDENCIES="
    build:
        virtual/pkg-config
        sys-devel/bison
        sys-devel/flex
    build+run:
        dev-libs/glib:2
        media-libs/freetype:2
        media-libs/libogg[>=1.2.0]
        media-libs/libpng[>=1.2.43]
        media-libs/libtheora[>=1.1.1]
        media-libs/libvorbis[>=1.3.1]
        media-libs/SDL:0[>=1.2]
        x11-libs/gtk+:2
        x11-libs/libX11
        alsa? ( sys-sound/alsa-lib )
        exult-studio? ( gnome-platform/libglade:2 )
        fluidsynth? ( media-sound/fluidsynth[>=1.1.1] )
        gimp-plugin? ( media-gfx/gimp[>=1.3.12] )
        opengl? ( x11-dri/mesa )
"

DEFAULT_SRC_CONFIGURE_PARAMS=(
    --enable-compiler
    --enable-data
    --enable-exult-studio-support
    --enable-mods
    --enable-nxbr
    --enable-shared
    --enable-zip-support
    --disable-pedantic-warnings
    --disable-static
)

DEFAULT_SRC_CONFIGURE_OPTION_ENABLES=(
    alsa
    debug
    "debug heavy-gdb-debug"
    exult-studio
    fluidsynth
    gimp-plugin
    mt32emu
    opengl
    "timidity timidity-midi"
)

WORK=${WORKBASE}/${PN}

src_prepare() {
    edo sed -i -e "/PKG_PROG_PKG_CONFIG/d" configure.ac

    export PKG_CONFIG=$(exhost --tool-prefix)pkg-config

    autotools_src_prepare
}

