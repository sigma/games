# Copyright 2016 Julian Ospald <hasufell@posteo.de>
# Distributed under the terms of the GNU General Public License v2

require alternatives

SUMMARY="A flexible, cross-platform scripting library"
HOMEPAGE="https://www.angelcode.com/angelscript/"
DOWNLOADS="https://www.angelcode.com/angelscript/sdk/files/angelscript_${PV}.zip"

LICENCES="ZLIB"
SLOT="0"
PLATFORMS="~amd64 ~x86"
MYOPTIONS="doc"

DEPENDENCIES="
    build:
        virtual/unzip
    run:
        !dev-libs/angelscript:0[<2.32.0-r1] [[
            description = [ Alternatives conflict ]
            resolution = upgrade-blocked-before
        ]]
"

WORK="${WORKBASE}"/sdk

src_compile() {
    emake -C ${PN}/projects/gnuc \
        LIBRARYDEST=
}

src_install() {
    emake -C ${PN}/projects/gnuc \
        DESTDIR="${IMAGE}" \
        LIBDIR_DEST=usr/$(exhost --target)/lib \
        INCLUDEDIR_DEST=usr/$(exhost --target)/include/ \
        DOCDIR_BASEDIR=usr/share/docs/${PNVR} \
        install_header install_shared

    if option doc ; then
        emake -C ${PN}/projects/gnuc \
            DESTDIR="${IMAGE}" \
            DOCDIR_BASEDIR=usr/share/docs/${PNVR} \
            install_docs
    fi

    local alternatives=()
    local host=$(exhost --target)

    alternatives=(
        /usr/${host}/include/${PN}.h ${PN}-${SLOT}.h
        /usr/${host}/lib/lib${PN}.so lib${PN}-${SLOT}.so
    )

    alternatives_for _${host}_${PN} ${SLOT} ${SLOT} "${alternatives[@]}"
}

