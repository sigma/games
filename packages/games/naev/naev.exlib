# Copyright 2021 Heiko Becker <heirecka@exherbo.org>
# Distributed under the terms of the GNU General Public License v2

require github [ pnv="${PNV}-source" release=v${PV} suffix="tar.xz" ] meson
require python [ blacklist=2 multibuild=false ]
require gtk-icon-cache

export_exlib_phases src_prepare

SUMMARY="A game about space exploration, trade and combat"
HOMEPAGE="https://naev.org/"

LICENCES="
    GPL-3 [[ note = [ C and Lua code ] ]]
    (
        CC0
        CCPL-Attribution-3.0
        CCPL-Attribution-ShareAlike-3.0
        CCPL-Attribution-ShareAlike-4.0
        GPL-2
        GPL-3
        public-domain
    ) [[ note = [ artwork, sounds ] ]]
    CCPL-Attribution-ShareAlike-4.0 [[ note = [ everythin else ] ]]
"
SLOT="0"
MYOPTIONS="
    doc
"

DEPENDENCIES="
    build:
        dev-libs/appstream
        dev-python/PyYAML[python_abis:*(-)?]
        sys-devel/gettext
        virtual/pkg-config
        doc? (
            app-doc/doxygen
            app-doc/ldoc
        )
    build+run:
        dev-games/physfs
        dev-lang/lua:5.1
        dev-libs/libunibreak[>=4.0]
        dev-libs/libxml2:2.0
        media-libs/freetype:2
        media-libs/libogg
        media-libs/libpng:=
        media-libs/libvorbis
        media-libs/libwebp:=
        media-libs/openal
        media-libs/SDL:2
        media-libs/SDL_image:2
        sci-libs/glpk
        sci-libs/OpenBLAS
        sci-libs/SuiteSparse
    test:
        x11-utils/xvfb-run
"
# NOTE: It also searches for mutagen, but AFAICT it's currently not used by
#       the meson build.

if ever at_least 0.10.0-alpha ; then
    DEPENDENCIES+="
        build+run:
            dev-libs/pcre2
    "
fi

# Both tests fail. One we could probably salvage, the other is some warnings
# about appstream files, which we probably don't care that much about.
RESTRICT="test"

MESON_SOURCE="${WORKBASE}"/${PNV}

MESON_SRC_CONFIGURE_PARAMS+=(
    -Dblas=openblas
    -Ddebug=false
    # Crashes with LuaJJIT on start, possibly because we set
    # -DLUAJIT_ENABLE_LUA52COMPAT.
    -Dluajit=disabled
)
MESON_SRC_CONFIGURE_OPTION_FEATURES+=(
    'doc docs_c'
    'doc docs_lua'
)

naev_src_prepare() {
    meson_src_prepare

    # Upstream lua doesn't provide a .pc file. As a result different names
    # are used in the wild and apparently we picked the wrong one for naev.
    edo sed -e "/dependency/s/lua51/lua-5.1/" -i meson.build

    # meson doesn't have a notion of docdic
    edo sed -e "s:doc/naev:doc/${PNVR}:" \
        -i meson.build \
        -i docs/meson.build

    edo sed \
        -e "s:/usr/bin/env python3:/usr/$(exhost --target)/bin/python$(python_get_abi):" \
        -i utils/build/gen_authors.py
}

