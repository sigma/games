Title: widelands update needs --permit-downgrade
Author: Heiko Becker <heirecka@exherbo.org>
Content-Type: text/plain
Posted: 2021-10-15
Revision: 1
News-Item-Format: 1.0
Display-If-Installed: games-strategy/widelands[>=15]

Widelands 1.0 was released. However, previous "Build xy" versions should have
been something similar to "widelands-0.xy". As a result of this, you need to
specify "--permit-downgrade games-strategy/widelands" if you have e.g.
widelands[=21] installed:

cave resolve games-strategy/widelands --permit-downgrade games-strategy/widelands
